MiraOS for RuuviTag
===================

An example of running the MiraOS on RuuviTag hardware, to show how commissioning
and status can be made using NFC

This integration is work in progress, and has not yet been fully developed in
respect of power consumption optimizations of the sensors or functionality of
integration with receiving sensor values

The code is delivered without guarantees, and is intended as an example.